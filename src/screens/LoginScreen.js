import React from 'react';
import './LoginScreen.css';

function LoginScreen() {
  return (
    <div className="login-screen">
      <div className="login-screen__background">
        <img
          className="login-screen__logo"
          src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png"
          alt=""
        />
        <button className="login-screen__button">Sign In</button>
        <div className="login-screen__gradient" />
      </div>
      <div className="login-screen__body">
        <>
          <h1>Unlimited films, TV programmes and more.</h1>
          <h2>Watch anywhere. Cancel at any time.</h2>
          <h3>
            Ready to watch? Enter your email to create or restart your
            membership.
          </h3>
          <div className="login-screen__input">
            <form>
              <input type="email" placeholder="Email Address" />
              <button className="login-screen__c2a">GET STARTED</button>
            </form>
          </div>
        </>
      </div>
    </div>
  );
}

export default LoginScreen;
